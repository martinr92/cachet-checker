package main

import (
	"encoding/json"
	"io/ioutil"
	"log"
)

// Setting is the main object for application settings.
type Setting struct {
	System     *SettingSystem     `json:"system,omitempty"`
	Validators []SettingValidator `json:"validators,omitempty"`
}

// SettingSystem contains cachet system informations.
type SettingSystem struct {
	BaseURL    string `json:"baseURL"`
	APIVersion int    `json:"apiVersion"`
	Token      string `json:"token"`
}

// SettingValidator is a single validator.
type SettingValidator struct {
	Component     *SettingsComponent     `json:"component,omitempty"`
	HTTPValidator *SettingsHTTPValidator `json:"httpValidator,omitempty"`
}

// SettingsComponent contains all data used to identify one component.
type SettingsComponent struct {
	ID        int    `json:"id,omitempty"`
	Name      string `json:"name,omitempty"`
	GroupID   int    `json:"groupId,omitempty"`
	GroupName string `json:"groupName,omitempty"`
}

// SettingValidatorInterface contains all global validator fields.
type SettingValidatorInterface interface {
	GetInterval() int
}

// SettingsHTTPValidator is a validator from type HTTP.
// If the timeout is reached, the component status changes to "partial outage".
// The performance timeout is optional and can be used to set a "performance issue" status on a component.
// E.g. you can choose a timeout of 10 seconds (10000), but a performance timeout already after 0.5s (500).
type SettingsHTTPValidator struct {
	URL                string `json:"url"`
	ResponseStatus     int    `json:"responseStatus"`
	Interval           int    `json:"interval"`
	Timeout            int    `json:"timeout"`
	PerformanceTimeout int    `json:"performanceTimeout,omitempty"`
}

// GetInterval returns the execution interval in milliseconds.
func (settingsHTTPValidator SettingsHTTPValidator) GetInterval() int {
	return settingsHTTPValidator.Interval
}

// NewSettingByPath reads the settings json file and parses this into the structs.
func NewSettingByPath(path string) Setting {
	// read settings file
	data, err := ioutil.ReadFile(path)
	if err != nil {
		log.Println("unable to read settings file, try to create one", err)
		settings := &Setting{}
		settings.Save(path)
		// TODO: check for a successfully saved file
	}

	// parse settings json
	var settings Setting
	json.Unmarshal(data, &settings)
	return settings
}

// Save stores the settings to the file system.
func (setting *Setting) Save(path string) {
	// TODO: save settings to json file
}
