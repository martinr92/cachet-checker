package main

import "testing"

func TestSuccess(t *testing.T) {
	// sample setting
	setting := SettingsHTTPValidator{URL: "https://www.google.com", ResponseStatus: 200}

	// initialize checker
	check := HTTPCheck{}
	check.Configure(setting)

	// run checker
	if ok, err := check.Validate(); !ok || err != nil {
		t.Error("HTTP call should run without errors", ok, err)
	}
}

func TestUnavailable(t *testing.T) {
	// sample setting
	setting := SettingsHTTPValidator{URL: "https://www.this-host-should-never-exists.abc", ResponseStatus: 200}

	// initialize checker
	check := HTTPCheck{}
	check.Configure(setting)

	// run checker
	if ok, err := check.Validate(); ok || err == nil {
		t.Error("HTTP call should fail", ok, err)
	}
}

func TestInvalidStatusCode(t *testing.T) {
	// sample setting
	setting := SettingsHTTPValidator{URL: "https://www.google.com", ResponseStatus: 123}

	// initialize checker
	check := HTTPCheck{}
	check.Configure(setting)

	// run checker
	if ok, err := check.Validate(); ok || err != nil {
		t.Error("HTTP status code validation failed", ok, err)
	}
}
