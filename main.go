package main

import (
	"flag"
	"os"
)

const (
	commandProcess = "process"
	commandConfig  = "config"
)

var (
	// processor: background processing service
	flagSetProcessor = flag.NewFlagSet(commandProcess, flag.ExitOnError)
	configPath       = flagSetProcessor.String("config", "config.json", "path and file name of the configuration file")

	// configurator: command for configuration
	flagSetConfigurator = flag.NewFlagSet(commandConfig, flag.ExitOnError)
)

func main() {
	parseCommand()
}

func parseCommand() {
	// check, if the main command was entered
	if len(os.Args) <= 1 {
		panic("missing command") // TODO: replace with error/flag documentation
	}

	// parse flagset
	switch os.Args[1] {
	case commandConfig:
		flagSetConfigurator.Parse(os.Args)
		break
	case commandProcess:
		flagSetProcessor.Parse(os.Args)
		settings := loadSettings()
		checker := NewCheckBrain(settings)
		checker.Start()
		break
	}
}

func loadSettings() Setting {
	return NewSettingByPath(*configPath)
}
