package main

import "time"

// CheckBrain is the main object use for the whole validation and timer handling.
type CheckBrain struct {
	Settings Setting
	Checks   []Check
}

// Check contains all information needed for a single validation.
type Check struct {
	SettingValidator    SettingValidator
	CheckImplementation CheckImplementation
	Ticker              *time.Ticker
	ExitChan            chan bool
}

// CheckImplementation is a interface with method that must be provided for each check implementation.
type CheckImplementation interface {
	Configure(configuration SettingValidatorInterface)
	Validate() (bool, error)
}

// NewCheckBrain creates a new Checker instance.
func NewCheckBrain(settings Setting) *CheckBrain {
	return &CheckBrain{Settings: settings}
}

// Start initializes all validations asynchronously.
func (checkBrain *CheckBrain) Start() {
	for _, validator := range checkBrain.Settings.Validators {
		if validator.HTTPValidator != nil {
			checkBrain.initValidator(validator, validator.HTTPValidator, &HTTPCheck{})
		}
	}
}

func (checkBrain *CheckBrain) initValidator(settingValidator SettingValidator, settingCheck SettingValidatorInterface,
	checkImplementation CheckImplementation) {
	// initial configuration of check implementation
	checkImplementation.Configure(settingCheck)

	// build new ticker and check object for holding reference
	check := Check{SettingValidator: settingValidator}
	check.Ticker = time.NewTicker(time.Millisecond * time.Duration(settingCheck.GetInterval()))
	checkBrain.Checks = append(checkBrain.Checks, check)

	// start thread for validation
	go checkBrain.check(check)
}

func (checkBrain *CheckBrain) check(check Check) {
	for {
		select {
		case <-check.ExitChan:
			return
		case <-check.Ticker.C:
			check.CheckImplementation.Validate()
			// TODO: validate response
			break
		}
	}
}

// Stop quits all validation tickers.
func (checkBrain *CheckBrain) Stop() {
	for _, check := range checkBrain.Checks {
		check.ExitChan <- true
	}
}
