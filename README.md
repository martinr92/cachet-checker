# cachet-checker
[![pipeline status](https://gitlab.com/martinr92/cachet-checker/badges/master/pipeline.svg)](https://gitlab.com/martinr92/cachet-checker/commits/master)
[![coverage report](https://gitlab.com/martinr92/cachet-checker/badges/master/coverage.svg)](https://gitlab.com/martinr92/cachet-checker/commits/master)
[![codecov](https://codecov.io/gl/martinr92/cachet-checker/branch/master/graph/badge.svg)](https://codecov.io/gl/martinr92/cachet-checker)
[![Go Report Card](https://goreportcard.com/badge/gitlab.com/martinr92/cachet-checker)](https://goreportcard.com/report/gitlab.com/martinr92/cachet-checker)

Cachet Checker is a command line application that monitors your infrastructure and updated the CachetHQ status page.

# cachet (golang framework)
[![GoDoc](https://godoc.org/gitlab.com/martinr92/cachet-checker?status.svg)](https://godoc.org/gitlab.com/martinr92/cachet-checker/cachet)
[![Apache License 2.0](https://img.shields.io/badge/license-Apache%202.0-blue.svg)](https://gitlab.com/martinr92/cachet-checker/blob/master/cachet/LICENSE)

This repository contains also the plain Cachet API implementation that can be used for your own golang application.
## Initialize Cachet Main Struct
The `Cachet`object is used for the whole server communication and can be initialized as follow:
```golang
import "gitlab.com/martinr92/cachet-checker/cachet"
c := cachet.NewCachet("https://host.name", 1, "api-token")
```

## Executing API calls
```golang
component, err := c.ReadComponentByID(1)
```
All methods are documented [here](https://godoc.org/gitlab.com/martinr92/cachet-checker/cachet) or you might have a look into the `*_test.go` files.

## Custom Logger
The framework prints debug information to `os.Stdout`. If you not want this is your project you might override the property `Log` of your cachet instance.

## License
```
Copyright 2018-2019 Martin Riedl

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

	http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
```
