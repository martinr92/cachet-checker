package main

import (
	"testing"
)

const (
	exampleSettingName = "config.example.json"
)

func TestLoadSettings(t *testing.T) {
	set := NewSettingByPath(exampleSettingName)

	// validate system data
	if set.System.APIVersion != 1 {
		t.Error("invalid data loading from settings file")
	}

	// check validator
	if set.Validators[0].HTTPValidator.GetInterval() != 60000 {
		t.Error("invalid value for HTTP validator interval")
	}
}

func TestNonExistingSettingsFile(t *testing.T) {
	_ = NewSettingByPath("non-existing-file.json")
}
