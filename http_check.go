package main

import (
	"net/http"
	"time"
)

// HTTPCheck is used for HTTP compatible URL calls and measures the time and response of each call.
type HTTPCheck struct {
	URL                string
	Method             string
	ResponseStatusCode int
	Client             *http.Client
}

// Configure prepares the object once.
func (httpCheck *HTTPCheck) Configure(configuration SettingValidatorInterface) {
	// cast setting object
	settings := configuration.(SettingsHTTPValidator)

	// parse settings
	httpCheck.URL = settings.URL
	httpCheck.ResponseStatusCode = settings.ResponseStatus

	// initialize http stuff
	httpCheck.Client = &http.Client{Timeout: time.Millisecond * time.Duration(settings.Timeout)}
}

// Validate is executed each time when a new validation is required.
func (httpCheck *HTTPCheck) Validate() (bool, error) {
	req, err := http.NewRequest(httpCheck.Method, httpCheck.URL, nil)
	if err != nil {
		return false, err
	}

	// run HTTP call
	resp, err := httpCheck.Client.Do(req)
	if err != nil {
		return false, err
	}

	// validate response status code
	if resp.StatusCode != httpCheck.ResponseStatusCode {
		return false, nil
	}

	return true, nil
}
