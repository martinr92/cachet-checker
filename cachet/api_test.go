// Copyright 2018-2019 Martin Riedl
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// 	http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package cachet_test

import (
	"net/http"
	"strconv"
	"strings"
	"testing"
	"time"

	"gitlab.com/martinr92/cachet-checker/cachet"
)

const (
	baseURL      = "https://demo.cachethq.io"
	token        = "9yMHsdioQosnyVK4iCVR"
	authUser     = "test@test.com"
	authPassword = "test123"
)

var oneDayAgo = time.Now().AddDate(0, 0, -1)

func TestInvalidHost(t *testing.T) {
	c := cachet.NewCachet("https://not-existing.hostname", 1, token)
	_, err := c.ReadComponentByID(1)
	if err == nil {
		t.Error("expected error for invalid hostname")
	}
}

func TestInvalidLocation(t *testing.T) {
	c := cachet.NewCachet(baseURL+"/not/exists", 1, token)
	_, err := c.Ping()
	if err == nil {
		t.Error("expected error for invalid base URL")
	}
	httpStatusError, ok := err.(*cachet.HTTPStatusError)
	if !ok {
		t.Error("expected invalid http response error")
	}
	if httpStatusError.Response.StatusCode != http.StatusNotFound {
		t.Error("unexpected http status code", httpStatusError.Response.StatusCode)
	}
	if !strings.Contains(httpStatusError.Error(), strconv.Itoa(http.StatusNotFound)) {
		t.Error("missing http status code in error message")
	}
}

func TestPing(t *testing.T) {
	c := cachet.NewCachet(baseURL, 1, token)
	response, err := c.Ping()
	if err != nil {
		t.Error(err)
	}
	expectedResponse := "Pong!"
	if response.Data != expectedResponse {
		t.Errorf("response value is '%s' instead of '%s'", response.Data, expectedResponse)
	}
}

func TestVersion(t *testing.T) {
	c := cachet.NewCachet(baseURL, 1, token)
	response, err := c.Version()
	if err != nil {
		t.Error(err)
	}

	// validate response data
	if response.Data == "" {
		t.Errorf("missing response value 'data'")
	}
	if response.Meta.Latest.TagName == "" {
		t.Errorf("missing latest version tag name")
	}
}
