// Copyright 2018-2019 Martin Riedl
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// 	http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package cachet

import "time"

// ComponentGroup object.
type ComponentGroup struct {
	ID        int     `json:"id"`
	Name      string  `json:"name"`
	CreatedAt *string `json:"created_at,omitempty"`
	UpdatedAt *string `json:"updated_at,omitempty"`
	Order     *int    `json:"order,omitempty"`
}

type readComponentGroupResponse struct {
	Data *ComponentGroup `json:"data,omitempty"`
}

// GetCreatedAtTime returns the parsed created timestamp.
func (componentGroup *ComponentGroup) GetCreatedAtTime() (time *time.Time, err error) {
	return parseTimeField(componentGroup.CreatedAt)
}

// GetUpdatedAtTime returns the parsed updated timestamp.
func (componentGroup *ComponentGroup) GetUpdatedAtTime() (time *time.Time, err error) {
	return parseTimeField(componentGroup.UpdatedAt)
}

// ReadComponentGroupByID reads a single component group using its ID.
// If the ID doesn't exist, nil is returned for component and for error.
func (cachet *Cachet) ReadComponentGroupByID(id int) (*ComponentGroup, error) {
	var response readComponentGroupResponse
	found, err := cachet.baseRead(methodComponents, id, &response)
	if !found {
		return nil, err
	}
	return response.Data, err
}
