// Copyright 2018-2019 Martin Riedl
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// 	http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package cachet_test

import (
	"testing"

	"gitlab.com/martinr92/cachet-checker/cachet"
)

func TestComponent(t *testing.T) {
	// read by ID
	c := cachet.NewCachet(baseURL, 1, token)
	component, err := c.ReadComponentByID(1)
	if err != nil {
		t.Error(err)
	}
	if component == nil {
		t.Error("component not found")
	}

	// validate fields
	if created, _ := component.GetCreatedAtTime(); created == nil || created.Before(oneDayAgo) {
		t.Error("invalid created time", created, oneDayAgo)
	}
	if updated, _ := component.GetUpdatedAtTime(); updated == nil || updated.Before(oneDayAgo) {
		t.Error("invalid updated time", updated)
	}
	if deleted, _ := component.GetDeletedAtTime(); deleted != nil {
		t.Error("invlaid deleted time", deleted)
	}

	// read by non-existing id
	component, err = c.ReadComponentByID(99999)
	if component != nil || err != nil {
		t.Error("unexpected response", component, err)
	}
}
