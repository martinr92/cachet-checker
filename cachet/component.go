// Copyright 2018-2019 Martin Riedl
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// 	http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package cachet

import (
	"time"
)

// Component object.
type Component struct {
	ID          int               `json:"id"`
	Name        string            `json:"name"`
	Description string            `json:"description"`
	Link        string            `json:"link"`
	Status      ComponentStatus   `json:"status"`
	Order       int               `json:"order"`
	GroupID     int               `json:"group"`
	CreatedAt   *string           `json:"created_at"`
	UpdatedAt   *string           `json:"updated_at"`
	DeletedAt   *string           `json:"deleted_at"`
	Enabled     bool              `json:"enabled"`
	StatusName  string            `json:"status_name"`
	Tags        map[string]string `json:"tags"`
}

// ComponentStatus as documented here https://docs.cachethq.io/docs/component-statuses
type ComponentStatus int

const (
	// ComponentStatusOperational The component is working.
	ComponentStatusOperational ComponentStatus = 1
	// ComponentStatusPerformanceIssues The component is experiencing some slowness.
	ComponentStatusPerformanceIssues ComponentStatus = 2
	// ComponentStatusPartialOutage The component may not be working for everybody.
	// This could be a geographical issue for example.
	ComponentStatusPartialOutage ComponentStatus = 3
	// ComponentStatusMajorOutage The component is not working for anybody.
	ComponentStatusMajorOutage ComponentStatus = 4
)

type readComponentResponse struct {
	Data *Component `json:"data,omitempty"`
}

// FindComponentResponse contains metadata and the components (data).
type FindComponentResponse struct {
	Meta *FindComponentMeta `json:"meta,omitempty"`
	Data []Component        `json:"data,omitempty"`
}

// FindComponentMeta contains metadata for the component find/search method.
type FindComponentMeta struct {
	Pagination MetaPagination `json:"pagination"`
}

// GetCreatedAtTime returns the parsed created timestamp.
func (component *Component) GetCreatedAtTime() (time *time.Time, err error) {
	return parseTimeField(component.CreatedAt)
}

// GetUpdatedAtTime returns the parsed updated timestamp.
func (component *Component) GetUpdatedAtTime() (time *time.Time, err error) {
	return parseTimeField(component.UpdatedAt)
}

// GetDeletedAtTime returns the parsed deleted timestamp.
func (component *Component) GetDeletedAtTime() (time *time.Time, err error) {
	return parseTimeField(component.DeletedAt)
}

// ReadComponentByID reads a single component using its ID.
// If the ID doesn't exist, nil is returned for component and for error.
func (cachet *Cachet) ReadComponentByID(id int) (*Component, error) {
	var response readComponentResponse
	found, err := cachet.baseRead(methodComponents, id, &response)
	if !found {
		return nil, err
	}
	return response.Data, err
}
