// Copyright 2018-2019 Martin Riedl
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// 	http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package cachet

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strconv"
	"time"
)

// Cachet is the main object of the CachetHQ API implementation based on
// https://docs.cachethq.io/reference
type Cachet struct {
	BaseURL    string
	APIVersion int
	Token      string
	Log        *log.Logger
}

const (
	methodPing            = "ping"
	methodVersion         = "version"
	methodComponents      = "components"
	methodComponentGroups = "components/groups"

	headerToken = "X-Cachet-Token"
)

// MetaPagination contains pagination metadata of most API responses.
type MetaPagination struct {
	Total       int                  `json:"total"`
	Count       int                  `json:"count"`
	PerPage     int                  `json:"per_page"`
	CurrentPage int                  `json:"current_page"`
	TotalPages  int                  `json:"total_pages"`
	Links       *MetaPaginationLinks `json:"links,omitempty"`
}

// MetaPaginationLinks contains the URLs used for pagination.
type MetaPaginationLinks struct {
	NextPage     *string `json:"next_page,omitempty"`
	PreviousPage *string `json:"previous_page,omitempty"`
}

// PingResponse contains all response data from a ping request.
type PingResponse struct {
	Data string `json:"data"`
}

// VersionResponse contains all response data from the version request.
type VersionResponse struct {
	Meta VersionMeta `json:"meta"`
	Data string      `json:"data"`
}

// VersionMeta contains data regarding the installed version.
type VersionMeta struct {
	IsLatest bool          `json:"on_latest"`
	Latest   VersionLatest `json:"latest"`
}

// VersionLatest contains the latest version.
type VersionLatest struct {
	TagName  string `json:"tag_name"`
	Prelease bool   `json:"prelease"`
	Draft    bool   `json:"draft"`
}

// NewCachet creates a new cachet instance with all required parameters.
func NewCachet(baseURL string, apiVersion int, token string) *Cachet {
	cachet := &Cachet{
		BaseURL:    baseURL,
		APIVersion: apiVersion,
		Token:      token,
		Log:        log.New(os.Stdout, "cachet", log.LstdFlags),
	}
	return cachet
}

// Ping sends a ping request to the cachet instance.
func (cachet *Cachet) Ping() (PingResponse, error) {
	var response PingResponse
	err := cachet.createRequest(http.MethodGet, methodPing, false, &response)
	return response, err
}

// Version requests the Cachet version information.
func (cachet *Cachet) Version() (VersionResponse, error) {
	var response VersionResponse
	err := cachet.createRequest(http.MethodGet, methodVersion, false, &response)
	return response, err
}

// HTTPStatusError is returned, if the server returned an invalid HTTP status code.
type HTTPStatusError struct {
	Response *http.Response
}

func (httpStatusError *HTTPStatusError) Error() string {
	return "invad http response status code " + strconv.Itoa(httpStatusError.Response.StatusCode)
}

func (cachet *Cachet) createRequest(method string, url string, useAuthentication bool, responseClass interface{}) error {
	requestURL := cachet.BaseURL + "/api/v" + strconv.Itoa(cachet.APIVersion) + "/" + url
	cachet.Log.Println(requestURL)
	request, err := http.NewRequest(method, requestURL, nil)
	if err != nil {
		return err
	}

	// send authentication header (if required)
	if useAuthentication {
		request.Header.Add(headerToken, cachet.Token)
	}

	// execute http call
	response, err := http.DefaultClient.Do(request)
	if err != nil {
		return err
	}

	// validate http response code
	responseStatus := response.StatusCode
	if method == http.MethodGet && responseStatus != http.StatusOK {
		return &HTTPStatusError{Response: response}
	}

	// prase response text
	responseData, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return err
	}

	// json marshalling into correct object
	cachet.Log.Println("response string:", string(responseData))
	return json.Unmarshal(responseData, responseClass)
}

func (cachet *Cachet) baseRead(path string, id int, responseClass interface{}) (found bool, err error) {
	err = cachet.createRequest(http.MethodGet, path+"/"+strconv.Itoa(id), false, &responseClass)

	// check for "not found"
	if err != nil {
		if httpError, ok := err.(*HTTPStatusError); ok && httpError.Response.StatusCode == http.StatusNotFound {
			return false, nil
		}
		return false, err
	}

	return true, nil
}

const timeFormat = "2006-1-2 15:04:05"

func parseTimeField(source *string) (*time.Time, error) {
	if source == nil {
		return nil, nil
	}

	time, err := time.Parse(timeFormat, *source)
	return &time, err
}
